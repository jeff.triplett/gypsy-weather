from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.template.defaultfilters import slugify
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


# Abstract models

class CreationModel(models.Model):
    created = models.DateTimeField(default=timezone.now, blank=True)
    updated = models.DateTimeField(blank=True)

    class Meta(object):
        abstract = True

    def save(self, *args, **kwargs):
        self.updated = timezone.now()
        return super(CreationModel, self).save(*args, **kwargs)


class BasicForecastModel(CreationModel):
    location = models.ForeignKey('Location', related_name='+')
    time = models.DateTimeField(blank=True)
    icon = models.CharField(max_length=100, blank=True)
    summary = models.CharField(max_length=100, blank=True)
    precip_intensity = models.DecimalField(max_digits=6, decimal_places=3, blank=True, null=True, default=0,
        help_text=_('A numerical value representing the intensity (in inches of liquid water per hour) of precipitation occurring at the given time conditional on probability (that is, assuming any precipitation occurs at all)'))
    precip_probability = models.DecimalField(max_digits=6, decimal_places=3, blank=True, null=True,
        help_text=_('The probability of precipitation occuring at the given time'))

    class Meta(object):
        abstract = True


class ForecastModel(BasicForecastModel):
    cloud_cover = models.CharField(max_length=100, blank=True, null=True,
        help_text=_('A numerical value between 0 and 1 (inclusive) representing the percentage of sky occluded by clouds.'))
    humidity = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    pressure = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True,
        help_text=_('A numerical value representing the air pressure in millibars.'))
    visibility = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True,
        help_text=_('A numerical value representing the average visibility in miles, capped at 10 miles.'))
    wind_bearing = models.IntegerField(blank=True, null=True,
        help_text=_('A numerical value representing the direction that the wind is coming from in degrees, with true north at 0 degrees and progressing clockwise.'))
    wind_speed = models.DecimalField(max_digits=6, decimal_places=2, blank=True, default=0,
        help_text=_('A numerical value representing the wind speed in miles per hour.'))
    # down ...
    apparent_temperature = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    dew_point = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True)
    ozone = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)

    class Meta(object):
        abstract = True


# App models

@python_2_unicode_compatible
class Location(CreationModel):
    name = models.CharField(max_length=100)
    slug = models.SlugField(blank=True)
    latitude = models.DecimalField(max_digits=11, decimal_places=6, blank=True)
    longitude = models.DecimalField(max_digits=11, decimal_places=6, blank=True)
    use_current = models.BooleanField(default=True)
    use_daily = models.BooleanField(default=True)
    use_hourly = models.BooleanField(default=True)
    use_minutely = models.BooleanField(default=True)
    use_forecast = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super(Location, self).save(*args, **kwargs)


@python_2_unicode_compatible
class Alert(CreationModel):
    location = models.ForeignKey(Location)
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    uri = models.CharField(max_length=1000, blank=True)
    time = models.DateTimeField(blank=True)
    expires = models.DateTimeField(blank=True)

    def __str__(self):
        return '{0}'.format(self.title)


@python_2_unicode_compatible
class CurrentCondition(ForecastModel):
    temperature = models.DecimalField(max_digits=5, decimal_places=2, blank=True)
    nearest_storm_distance = models.IntegerField(blank=True, null=True)
    nearest_storm_bearing = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return '{0} ({1})'.format(self.summary, self.temperature)


@python_2_unicode_compatible
class DailyForecast(ForecastModel):
    precip_type = models.CharField(max_length=30, blank=True)
    temperature_min = models.DecimalField(max_digits=5, decimal_places=2, blank=True)
    temperature_min_time = models.DateTimeField(blank=True)
    temperature_max = models.DecimalField(max_digits=5, decimal_places=2, blank=True)
    temperature_max_time = models.DateTimeField(blank=True)
    sunrise_time = models.DateTimeField(blank=True)
    sunset_time = models.DateTimeField(blank=True)

    def __str__(self):
        return '{0} (H: {1}; L: {2})'.format(self.summary, self.temperature_max, self.temperature_min)


@python_2_unicode_compatible
class HourlyForecast(ForecastModel):
    temperature = models.DecimalField(max_digits=5, decimal_places=2, blank=True)

    def __str__(self):
        return '{0} ({1})'.format(self.summary, self.temperature)


@python_2_unicode_compatible
class MinutelyForecast(BasicForecastModel):

    def __str__(self):
        return '{0}'.format(self.summary)


'''
class Timeline(models.Model):
    time = models.DateTimeField(blank=True)
    location = models.ForeignKey(Location, related_name='+')

    content_type = models.ForeignKey(ContentType, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    class Meta:
        ordering = ('-content_type',)
        verbose_name = _('Timeline')
        verbose_name_plural = _('Timelines')
'''
