from django.contrib import admin

from .models import Alert, Location, CurrentCondition, DailyForecast, HourlyForecast, MinutelyForecast


class DateModelAdmin(admin.ModelAdmin):

    def get_readonly_fields(self, request, obj=None):
        fields = list(super(DateModelAdmin, self).get_readonly_fields(request, obj))
        return fields + ['created', 'updated']

    def get_list_filter(self, request):
        fields = list(super(DateModelAdmin, self).get_list_filter(request))
        return fields + ['created', 'updated']

    def get_list_display(self, request):
        fields = list(super(DateModelAdmin, self).get_list_display(request))
        return fields + ['created']
        #return fields + ['created', 'updated']

    def get_fieldsets(self, request, obj=None):
        """
        Move the created/updated/creator fields to a fieldset of its own,
        at the end, and collapsed.
        """
        # Remove created/updated/creator from any existing fieldsets. They'll
        # be there if the child class didn't manually declare fieldsets.
        fieldsets = super(DateModelAdmin, self).get_fieldsets(request, obj)
        for name, fieldset in fieldsets:
            for f in ('created', 'updated'):
                if f in fieldset['fields']:
                    fieldset['fields'].remove(f)

        # Now add these fields to a collapsed fieldset at the end.
        # FIXME: better name than "CMS metadata", that sucks.
        return fieldsets + [("Metadata", {
            'fields': ['created', 'updated'],
            'classes': ('collapse',),
        })]


class AlertAdmin(DateModelAdmin):
    list_display = ['title', 'location', 'time', 'expires']
    list_filter = ['location']
    raw_id_fields = ['location']


class LocationAdmin(DateModelAdmin):
    list_display = ['name', 'latitude', 'longitude', 'use_current', 'use_daily', 'use_hourly', 'use_minutely', 'use_forecast']
    list_filter = ['use_current', 'use_daily', 'use_hourly', 'use_minutely', 'use_forecast']
    prepopulated_fields = {'slug': ('name',)}


class CurrentConditionAdmin(DateModelAdmin):
    list_display = ['__str__', 'temperature', 'location', 'time']
    list_filter = ['location']
    raw_id_fields = ['location']


class DailyForecastAdmin(DateModelAdmin):
    list_display = ['__str__', 'temperature_min', 'temperature_max', 'location', 'time']
    list_filter = ['location']
    raw_id_fields = ['location']


class HourlyForecastAdmin(DateModelAdmin):
    list_display = ['__str__', 'temperature', 'location', 'time']
    list_filter = ['location']
    raw_id_fields = ['location']


class MinutelyForecastAdmin(DateModelAdmin):
    list_display = ['__str__', 'time', 'location']
    list_filter = ['location']
    raw_id_fields = ['location']


admin.site.register(Alert, AlertAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.register(CurrentCondition, CurrentConditionAdmin)
admin.site.register(DailyForecast, DailyForecastAdmin)
admin.site.register(HourlyForecast, HourlyForecastAdmin)
admin.site.register(MinutelyForecast, MinutelyForecastAdmin)
