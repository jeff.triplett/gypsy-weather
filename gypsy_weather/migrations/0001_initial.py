# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Location'
        db.create_table(u'gypsy_weather_location', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50, blank=True)),
            ('latitude', self.gf('django.db.models.fields.DecimalField')(max_digits=11, decimal_places=6, blank=True)),
            ('longitude', self.gf('django.db.models.fields.DecimalField')(max_digits=11, decimal_places=6, blank=True)),
            ('use_current', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('use_daily', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('use_hourly', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('use_minutely', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('use_forecast', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'gypsy_weather', ['Location'])

        # Adding model 'Alert'
        db.create_table(u'gypsy_weather_alert', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['gypsy_weather.Location'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('uri', self.gf('django.db.models.fields.CharField')(max_length=1000, blank=True)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('expires', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
        ))
        db.send_create_signal(u'gypsy_weather', ['Alert'])

        # Adding model 'CurrentCondition'
        db.create_table(u'gypsy_weather_currentcondition', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', to=orm['gypsy_weather.Location'])),
            ('time', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('icon', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('summary', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('precip_intensity', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=6, decimal_places=3, blank=True)),
            ('cloud_cover', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('humidity', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=2, blank=True)),
            ('pressure', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=2, blank=True)),
            ('visibility', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=2, blank=True)),
            ('wind_bearing', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('wind_speed', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True)),
            ('temperature', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2, blank=True)),
        ))
        db.send_create_signal(u'gypsy_weather', ['CurrentCondition'])

        # Adding model 'DailyForecast'
        db.create_table(u'gypsy_weather_dailyforecast', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', to=orm['gypsy_weather.Location'])),
            ('time', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('icon', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('summary', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('precip_intensity', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=6, decimal_places=3, blank=True)),
            ('cloud_cover', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('humidity', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=2, blank=True)),
            ('pressure', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=2, blank=True)),
            ('visibility', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=2, blank=True)),
            ('wind_bearing', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('wind_speed', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True)),
            ('precip_type', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('temperature_min', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2, blank=True)),
            ('temperature_min_time', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('temperature_max', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2, blank=True)),
            ('temperature_max_time', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('sunrise_time', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('sunset_time', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
        ))
        db.send_create_signal(u'gypsy_weather', ['DailyForecast'])

        # Adding model 'HourlyForecast'
        db.create_table(u'gypsy_weather_hourlyforecast', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', to=orm['gypsy_weather.Location'])),
            ('time', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('icon', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('summary', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('precip_intensity', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=6, decimal_places=3, blank=True)),
            ('cloud_cover', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('humidity', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=2, blank=True)),
            ('pressure', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=2, blank=True)),
            ('visibility', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=2, blank=True)),
            ('wind_bearing', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('wind_speed', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=6, decimal_places=2, blank=True)),
            ('temperature', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2, blank=True)),
        ))
        db.send_create_signal(u'gypsy_weather', ['HourlyForecast'])

        # Adding model 'MinutelyForecast'
        db.create_table(u'gypsy_weather_minutelyforecast', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('updated', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('location', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', to=orm['gypsy_weather.Location'])),
            ('time', self.gf('django.db.models.fields.DateTimeField')(blank=True)),
            ('icon', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('summary', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('precip_intensity', self.gf('django.db.models.fields.DecimalField')(default=0, null=True, max_digits=6, decimal_places=3, blank=True)),
        ))
        db.send_create_signal(u'gypsy_weather', ['MinutelyForecast'])


    def backwards(self, orm):
        # Deleting model 'Location'
        db.delete_table(u'gypsy_weather_location')

        # Deleting model 'Alert'
        db.delete_table(u'gypsy_weather_alert')

        # Deleting model 'CurrentCondition'
        db.delete_table(u'gypsy_weather_currentcondition')

        # Deleting model 'DailyForecast'
        db.delete_table(u'gypsy_weather_dailyforecast')

        # Deleting model 'HourlyForecast'
        db.delete_table(u'gypsy_weather_hourlyforecast')

        # Deleting model 'MinutelyForecast'
        db.delete_table(u'gypsy_weather_minutelyforecast')


    models = {
        u'gypsy_weather.alert': {
            'Meta': {'object_name': 'Alert'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'expires': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gypsy_weather.Location']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'uri': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'})
        },
        u'gypsy_weather.currentcondition': {
            'Meta': {'object_name': 'CurrentCondition'},
            'cloud_cover': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'humidity': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'icon': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['gypsy_weather.Location']"}),
            'precip_intensity': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '6', 'decimal_places': '3', 'blank': 'True'}),
            'pressure': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'temperature': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2', 'blank': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'visibility': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'wind_bearing': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'wind_speed': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'})
        },
        u'gypsy_weather.dailyforecast': {
            'Meta': {'object_name': 'DailyForecast'},
            'cloud_cover': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'humidity': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'icon': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['gypsy_weather.Location']"}),
            'precip_intensity': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '6', 'decimal_places': '3', 'blank': 'True'}),
            'precip_type': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'pressure': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'sunrise_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'sunset_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'temperature_max': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2', 'blank': 'True'}),
            'temperature_max_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'temperature_min': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2', 'blank': 'True'}),
            'temperature_min_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'visibility': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'wind_bearing': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'wind_speed': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'})
        },
        u'gypsy_weather.hourlyforecast': {
            'Meta': {'object_name': 'HourlyForecast'},
            'cloud_cover': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'humidity': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'icon': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['gypsy_weather.Location']"}),
            'precip_intensity': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '6', 'decimal_places': '3', 'blank': 'True'}),
            'pressure': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'temperature': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2', 'blank': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'visibility': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'wind_bearing': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'wind_speed': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'})
        },
        u'gypsy_weather.location': {
            'Meta': {'object_name': 'Location'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '11', 'decimal_places': '6', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '11', 'decimal_places': '6', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'use_current': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'use_daily': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'use_forecast': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'use_hourly': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'use_minutely': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'gypsy_weather.minutelyforecast': {
            'Meta': {'object_name': 'MinutelyForecast'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'icon': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['gypsy_weather.Location']"}),
            'precip_intensity': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '6', 'decimal_places': '3', 'blank': 'True'}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'})
        }
    }

    complete_apps = ['gypsy_weather']