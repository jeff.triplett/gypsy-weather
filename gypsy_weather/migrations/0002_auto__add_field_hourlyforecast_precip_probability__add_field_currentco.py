# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'HourlyForecast.precip_probability'
        db.add_column(u'gypsy_weather_hourlyforecast', 'precip_probability',
                      self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=3, blank=True),
                      keep_default=False)

        # Adding field 'CurrentCondition.precip_probability'
        db.add_column(u'gypsy_weather_currentcondition', 'precip_probability',
                      self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=3, blank=True),
                      keep_default=False)

        # Adding field 'MinutelyForecast.precip_probability'
        db.add_column(u'gypsy_weather_minutelyforecast', 'precip_probability',
                      self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=3, blank=True),
                      keep_default=False)

        # Adding field 'DailyForecast.precip_probability'
        db.add_column(u'gypsy_weather_dailyforecast', 'precip_probability',
                      self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=6, decimal_places=3, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'HourlyForecast.precip_probability'
        db.delete_column(u'gypsy_weather_hourlyforecast', 'precip_probability')

        # Deleting field 'CurrentCondition.precip_probability'
        db.delete_column(u'gypsy_weather_currentcondition', 'precip_probability')

        # Deleting field 'MinutelyForecast.precip_probability'
        db.delete_column(u'gypsy_weather_minutelyforecast', 'precip_probability')

        # Deleting field 'DailyForecast.precip_probability'
        db.delete_column(u'gypsy_weather_dailyforecast', 'precip_probability')


    models = {
        u'gypsy_weather.alert': {
            'Meta': {'object_name': 'Alert'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'expires': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gypsy_weather.Location']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'uri': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'})
        },
        u'gypsy_weather.currentcondition': {
            'Meta': {'object_name': 'CurrentCondition'},
            'cloud_cover': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'humidity': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'icon': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['gypsy_weather.Location']"}),
            'precip_intensity': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '6', 'decimal_places': '3', 'blank': 'True'}),
            'precip_probability': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '3', 'blank': 'True'}),
            'pressure': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'temperature': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2', 'blank': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'visibility': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'wind_bearing': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'wind_speed': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'})
        },
        u'gypsy_weather.dailyforecast': {
            'Meta': {'object_name': 'DailyForecast'},
            'cloud_cover': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'humidity': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'icon': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['gypsy_weather.Location']"}),
            'precip_intensity': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '6', 'decimal_places': '3', 'blank': 'True'}),
            'precip_probability': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '3', 'blank': 'True'}),
            'precip_type': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'pressure': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'sunrise_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'sunset_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'temperature_max': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2', 'blank': 'True'}),
            'temperature_max_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'temperature_min': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2', 'blank': 'True'}),
            'temperature_min_time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'visibility': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'wind_bearing': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'wind_speed': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'})
        },
        u'gypsy_weather.hourlyforecast': {
            'Meta': {'object_name': 'HourlyForecast'},
            'cloud_cover': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'humidity': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'icon': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['gypsy_weather.Location']"}),
            'precip_intensity': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '6', 'decimal_places': '3', 'blank': 'True'}),
            'precip_probability': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '3', 'blank': 'True'}),
            'pressure': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'temperature': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2', 'blank': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'visibility': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'}),
            'wind_bearing': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'wind_speed': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '6', 'decimal_places': '2', 'blank': 'True'})
        },
        u'gypsy_weather.location': {
            'Meta': {'object_name': 'Location'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '11', 'decimal_places': '6', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'max_digits': '11', 'decimal_places': '6', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'use_current': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'use_daily': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'use_forecast': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'use_hourly': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'use_minutely': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'gypsy_weather.minutelyforecast': {
            'Meta': {'object_name': 'MinutelyForecast'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'icon': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': u"orm['gypsy_weather.Location']"}),
            'precip_intensity': ('django.db.models.fields.DecimalField', [], {'default': '0', 'null': 'True', 'max_digits': '6', 'decimal_places': '3', 'blank': 'True'}),
            'precip_probability': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '6', 'decimal_places': '3', 'blank': 'True'}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'blank': 'True'})
        }
    }

    complete_apps = ['gypsy_weather']