import arrow

from django.conf import settings
from django.core.management.base import BaseCommand
from optparse import make_option
from requests_forecast import Forecast

from ...models import Alert, Location, CurrentCondition, DailyForecast, HourlyForecast, MinutelyForecast


FORECAST_API_KEY = getattr(settings, 'FORECAST_API_KEY', '')


class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('-t', '--date', dest='date'),
        #make_option('-c', '--skip-current', action='store_true', dest='use_current'),
        #make_option('-d', '--skip-daily', action='store_true', dest='use_daily'),
        #make_option('-m', '--skip-minutely', action='store_true', dest='use_minutely'),
        #make_option('-s', '--skip-hourly', action='store_true', dest='use_hourly'),
    )

    def handle(self, *args, **options):
        date = options.pop('date', None)
        if date:
            date = arrow.get(date).datetime
            print 'date: {}'.format(date)

        locations = Location.objects.all().iterator()
        for location in locations:
            print '==location: {}'.format(location)
            self.get_forecast(location, date=date)

    def get_forecast(self, location, filename=None, date=None):
        forecast = Forecast(FORECAST_API_KEY)

        forecast.get(latitude=location.latitude,
            longitude=location.longitude, time=date)

        if location.use_current:
            current = forecast.get_currently()
            self.process_current_condition(location, current)

        if location.use_daily:
            daily = forecast.get_daily()
            self.process_daily_forecast(location, daily['data'])

        if location.use_hourly:
            hourly = forecast.get_hourly()
            self.process_hourly_forecast(location, hourly['data'])

        if location.use_minutely:
            minutely = forecast.get_minutely()
            self.process_minutely_forecast(location, minutely)

        # this is to catch alerts in case they happen. the api dosn't
        # allow for fetching historial alerts :/
        #latitude, longitude = '37.8267', '-122.423'
        #forecast.get(latitude=latitude, longitude=longitude)
        alerts = forecast.get_alerts()
        if alerts:
            self.process_alerts(location, alerts)

    def process_alerts(self, location, alert_data):
        for item in alert_data:
            time_converted = item['time']
            defaults = {
                'title': item['title'],
                'description': item['description'],
                'uri': item['uri'],
                'expires': item.get('expires'),
            }
            alert, _ = Alert.objects.get_or_create(
                location=location, time=time_converted, defaults=defaults)

    def process_current_condition(self, location, item):
        time_converted = item['time']
        defaults = {
            'apparent_temperature': item['apparentTemperature'],
            'cloud_cover': item['cloudCover'],
            'dew_point': item['dewPoint'],
            'humidity': item['humidity'],
            'icon': item['icon'],
            'nearest_storm_bearing': item['nearestStormBearing'],
            'nearest_storm_distance': item['nearestStormDistance'],
            'ozone': item['ozone'],
            'precip_intensity': item.get('precipIntensity'),
            'precip_probability': item.get('precipProbability'),
            'pressure': item['pressure'],
            'summary': item['summary'],
            'temperature': item['temperature'],
            'visibility': item.get('visibility', 0.0),
            'wind_bearing': item['windBearing'],
            'wind_speed': item['windSpeed'],
        }
        current_condition, _ = CurrentCondition.objects.get_or_create(
            location=location, time=time_converted, defaults=defaults)

    def process_daily_forecast(self, location, daily_data):
        for item in daily_data:
            time_converted = item['time']
            defaults = {
                #'apparent_temperature': item['apparentTemperature'],
                'cloud_cover': item['cloudCover'],
                'dew_point': item['dewPoint'],
                'humidity': item['humidity'],
                'icon': item['icon'],
                'ozone': item['ozone'],
                'precip_intensity': item.get('precipIntensity'),
                'precip_probability': item.get('precipProbability'),
                'precip_type': item.get('precipType', ''),
                'pressure': item['pressure'],
                'summary': item['summary'],
                'sunrise_time': item['sunriseTime'],
                'sunset_time': item['sunsetTime'],
                'temperature_max': item['temperatureMax'],
                'temperature_max_time': item['temperatureMaxTime'],
                'temperature_min': item['temperatureMin'],
                'temperature_min_time': item['temperatureMinTime'],
                'visibility': item.get('visibility', 0.0),
                'wind_bearing': item.get('windBearing'),
                'wind_speed': item['windSpeed'],
            }
            daily_forcast, _ = DailyForecast.objects.get_or_create(
                location=location, time=time_converted, defaults=defaults)

    def process_hourly_forecast(self, location, hourly_data):
        for item in hourly_data:
            time_converted = item['time']
            defaults = {
                'apparent_temperature': item['apparentTemperature'],
                'cloud_cover': item['cloudCover'],
                'dew_point': item['dewPoint'],
                'humidity': item['humidity'],
                'icon': item['icon'],
                'ozone': item['ozone'],
                'precip_intensity': item.get('precipIntensity'),
                'precip_probability': item.get('precipProbability'),
                'pressure': item['pressure'],
                'summary': item['summary'],
                'temperature': item['temperature'],
                'visibility': item.get('visibility', 0.0),
                'wind_bearing': item['windBearing'],
                'wind_speed': item['windSpeed'],
            }
            hourly_forcast, created = HourlyForecast.objects.get_or_create(
                location=location, time=time_converted, defaults=defaults)

    def process_minutely_forecast(self, location, minutely_data):
        if 'data' in minutely_data:
            icon = minutely_data.get('icon')
            summary = minutely_data.get('summary')

            for item in minutely_data['data']:
                time_converted = item['time']
                defaults = {
                    'icon': icon,
                    'precip_intensity': item.get('precipIntensity'),
                    'precip_probability': item.get('precipProbability'),
                    'summary': summary,
                }
                minutely_forcast, _ = MinutelyForecast.objects.get_or_create(
                    location=location, time=time_converted, defaults=defaults)
